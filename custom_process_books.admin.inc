<?php

function custom_process_books_form($form, &$form_state) {

    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Import options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['options']['custom_process_books_datadir'] = array(
      '#type' => 'textfield',
      '#title' => t('Directory in which to clone repositories'),
      '#default_value' => custom_variable_get('custom_process_books_datadir', ''),
      '#description' => 'A directory will be cloned inside this directory for each repository listed below.'
    );   
    $form['options']['custom_process_books_cutoff'] = array(
      '#type' => 'textfield',
      '#width' => 10,
      '#title' => t('Number of pages to process per batch'),
      '#default_value' => custom_variable_get('custom_process_books_cutoff', 3000),
      '#description' => 'A number larger than the number of pages in the largest book, but not so large that the server starts having issues.' 
    );   
    $form['options']['custom_process_books_sleep'] = array(
      '#type' => 'textfield',
      '#width' => 10,
      '#title' => t('Seconds to sleep after each batch'),
      '#default_value' => custom_variable_get('custom_process_books_sleep', 10),
      '#description' => 'Even though each new batch calls a new Apache request, the mysql daemon persists between batches and as a result gets hit pretty hard. Introducing a delay of a few seconds between batches can help mitigate this.' 
    ); 
    $repositories = implode("\n" , array_keys(custom_variable_get('custom_process_books_repositories', array())));
    $form['options']['custom_process_books_repositories'] = array(
      '#type' => 'textarea',
      '#title' => t('Repositories to get the books from'),
      '#default_value' => $repositories,
      '#description' => 'One repository per line. <ul><li>For remote repositories, list the ssh details (ie username@hostname:repodirectorypath/repository.git). If it is a private repository, make sure you have added the public key of the webuser account.</li><li>To test locally, create an empty git repository inside the folder containg the test files, and add the path to the folder here</li></ul>', 
    );      
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'options-submit',
      '#value' => t('Save Options'),
      '#executes_submit_callback' => TRUE,
      '#validate' => array('custom_process_books_options_validate'),
      '#submit' => array('custom_process_books_options_submit'),
    );
    $form['custom_process_books']['manual_update'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manual Update'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Manually run a batch update. For large jobs this should work, but using drush or leaving to cron is preferable. Make sure the options are saved first'),
    );

    $form['custom_process_books']['manual_update']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'manual-update-submit',
      '#value' => t('Start manual processing'),
      '#description' => t('Manually update a book in the library.'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('custom_process_books_manual_batch'),
    );
    return $form;
}

function custom_process_books_options_validate($form, &$form_state) {

  if (!is_dir($form_state['values']['custom_process_books_datadir']) && !drupal_mkdir($form_state['values']['custom_process_books_datadir'])) {
    form_set_error('custom_process_books_datadir', 'The directory does not exist and could not be created');
  }
}

function custom_process_books_options_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('custom_process_books_datadir', $values['custom_process_books_datadir']);
  variable_set('custom_process_books_cutoff', $values['custom_process_books_cutoff']);
  
  $existing_repo_data = custom_variable_get('custom_process_books_repositories', array());	
  $new_repo_keys = explode("\n", $values['custom_process_books_repositories']);
  
  $newarray = array();
  foreach ($new_repo_keys as $name) {
    $trimmed_name = trim($name);  
    $newarray[$trimmed_name] = !empty($existing_repo_data[$trimmed_name]) ? $existing_repo_data[$trimmed_name] : ''; 
    //drupal_set_message( "'" . $trimmed_name . "' : " . $newarray[$trimmed_name]);
  }    
  variable_set('custom_process_books_repositories', $newarray);

  drupal_set_message('The options have been saved.');
} 

function custom_process_books_manual_batch($form, &$form_state) {
	custom_process_books_prepare_batch();
}	      	 