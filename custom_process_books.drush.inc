<?php

/**
 * @file
 *   drush integration for custom_process_books.
 */

/*
 * Implements hook_drush_command().
 *
 * @return array
 *   An associative array describing your command(s).
 */
function custom_process_books_drush_command() {
  $items = array();

  $items['import-books'] = array(
    'callback' => 'custom_process_books_drush_import_books',
    'description' => dt('Import books.'),
    'arguments' => array(
      'reset' => dt('Whether to reset the current index and import books all over again'),
    ),
    /*'options' => array(
      'environment-id' => 'The environment ID',
    ),
    'examples' => array(
      'drush solr-delete-index node' => 'Delete all node content from the index.',
      'drush solr-delete-index node:article' => 'Delete all content of the article content type from the index.',
      'drush solr-delete-index node:article node:blog' => 'Delete all content of the article and blog content types from the index.',
    ), */
    'aliases' => array('ib'), 
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param string $section
 *   A string with the help section (prepend with 'drush:')
 * @return string
 *   A string with the help text for your command.
 */
function custom_process_books_drush_help($section) {
  switch ($section) {
    case 'drush:import-books':
      return dt("Imports books from library.");
  }
  return '';
}

function custom_process_books_drush_import_books($reset = NULL) {
  print 'Starting Drush process...' . "\n";
  if ($reset == 'reset') {
    variable_set('custom_process_currentindex', 0);
    print 'Resetting current index...' . "\n";
  }

  custom_process_books_prepare_batch(TRUE);
} 





