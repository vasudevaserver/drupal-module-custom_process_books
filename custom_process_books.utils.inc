<?php
/**
 * @file
 * Utility functions for Module: Custom Book Processing
 */

/*
 * Read the file contents into an array suitable for page-level parsing
 */
function custom_process_books_prepare_org_mode_file_contents($file_path) {

  $f = fopen($file_path, 'r');
    // load the raw text 
  $size = filesize($file_path);
  $return['SIZE'] = $size;
  $filetext = fread($f, $size);
  
  // If the file has an org-mode info bit at the end, chop that off
  if(strpos($filetext, '#+SETUPFILE:') > 0) {
    $filetext = substr($filetext , 0, strpos($filetext, '#+SETUPFILE:'));
  }  
  
  // Standardize the carriage return
  $filetext = str_replace(chr(13), chr(10), $filetext);
  
  // Pages are seperated by 2 line breaks, and then header beginning with org-mode *
  $delimiter = chr(10) . chr(10) . '*';
  $pages = explode($delimiter, $filetext);
  $return['PAGES'] = $pages;
  return $return;
}


// Retrieve the hash data for this file_id.
// db_query is faster than db_select
function custom_process_books_get_hash($nid) {
  return db_query("SELECT hash FROM {custom_process_books_nodehashes} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
}
 

# Retrieve a taxonomy term from the db
function custom_process_books_get_taxonomy_term($tterm, $vid) {
  //see if this term already exists and fetch it if it does
  $term = taxonomy_get_term_by_name($tterm);
 
  //if it doesn't exist, make it
  if ($term == array()){
    //make a new class to hold the term for taxonomy 1
    $taxonomy = new stdClass();
    $taxonomy->name = $tterm;
    $taxonomy->vid = $vid;
    taxonomy_term_save($taxonomy);
     //now fetch it so we have its tid
    $term = taxonomy_get_term_by_name($tterm);
  }
  return $term;
}



// Process the title of the page or book.
function custom_process_books_clean_up_title($text) {
  $title = custom_process_books_clean_up_entities($text);
  if (strlen($title) > CUSTOM_PROCESS_BOOKS_MAX_TITLE_SIZE ) {
    $title = substr($title, 
                  0,
                  CUSTOM_PROCESS_BOOKS_MAX_TITLE_SIZE);
    $ptitle = explode(' ', $title);
    array_pop($ptitle);
    $title = implode(' ', $ptitle);
    $title = trim($title);
  }
  return $title;
}


// Determine an appropriate path for the data provided
function custom_process_books_select_path($data) {
  
  $path = $data['PATH'];
  $language = isset($data['LANGUAGE']) ? $data['LANGUAGE'] : LANGUAGE_NONE;
  $return = $path;
  $max = CUSTOM_PROCESS_BOOKS_MAX_URL_SIZE;
  if (strlen($path) > $max) {
    $pathargs = explode('-', substr($path, 0, $max));
    unset($pathargs[count($pathargs) - 1]);
    $path = implode('-', $pathargs);
  }
  $source = '';
  if (array_key_exists('NID', $data)) {
    $source = 'node/' . trim($data['NID']);
  }
  if ($source) {
    $q = db_select('url_alias', 'u')
          ->fields('u')
          ->condition('alias', $path, '=')
          ->condition('source', $source, '<>')
          ->condition('language', $language)
          ->execute()
          ->fetchAssoc();
  }
  else {
    $q = db_select('url_alias', 'u')
          ->fields('u')
          ->condition('alias', $path, '=')
          ->condition('language', $language)
          ->execute()
          ->fetchAssoc();
  }
  if (!$q) {
    return $path;
  }
  // A path already exists !
  $i=0;
  $finished = FALSE;
  while (!$finished) {
    $i++;
    $new_path = $path . '-' . strval($i);
    $q = db_select('url_alias', 'u')
          ->fields('u')
          ->condition('alias', $new_path, '=')
          ->execute()
          ->fetchAssoc();
    if (!$q) {
      $finished = TRUE;
    }
  }
  return $new_path;
}


// Strip tags from the text
function custom_process_books_strip_tags($text, $tags) {
  $returntext = trim($text);
  $searchtext = strtolower($returntext);
  $taglist = array_keys($tags);
  $ctr = 0;
  while (TRUE) {
    if ($ctr > 100) {
      break;
    }
    $ctr++;
    $found = FALSE;
    foreach ($taglist as $tag) {
      $tlength = strlen($tag);
      $slength = strlen($searchtext);
      if (strpos($searchtext, $tag) === FALSE) {
        continue;
      } 
      $directives = $tags[$tag];
      if (in_array('a', $directives)){
        $returntext = str_replace($tag, '', $returntext);
        $searchtext = strtolower($returntext);
      }
      else {
        if (in_array('b', $directives)){
          // strip tag from beginning of text
          if (substr($searchtext, 0, $tlength) == $tag) {
            $found = TRUE;
            $returntext = trim(substr($returntext, $tlength));
            $searchtext = strtolower($returntext);
          }
        }
        if (in_array('e', $directives)){
          // strip tags from end of text
          $newlength = $slength - $tlength;
          if (substr($searchtext, $newlength, $tlength) == $tag) {
            $found = TRUE;
            $returntext = trim(substr($returntext, 0, $newlength));
            $searchtext = strtolower($returntext);
          }
        }
      }
    }
    if (!$found) {
      break;
    }
  }
  return $returntext;
}

/**
*
* Create clean urls from text
* 
*/
function custom_process_books_make_url_from_text($text, $book_data=Array()) {
  // Strip out spaces and other unusable characters from text.

  // Let's work with a copy of the text
  // move to lower case and strip leading/trailing whitespace
  $result = trim(strtolower($text));
  
  $illegalchars = custom_process_books_illegal_path_characters();

  // replace:
  $result = str_replace($illegalchars['replace'], '-', $result);
  // delete:
  $result = str_replace($illegalchars['delete'], '', $result);
 // double hyphen '--'
  $result = str_replace('--', '-', $result);
 
  // strip special characters
  $result = custom_process_books_strip_tags(
          $result,
          Array('-' => Array('b', 'e')));
  
  // Remove "part" from book url
  if (count($book_data) == 0) {
    if (strpos($result, '-part-') !== FALSE) {
      $bookargs = explode('-', $result);
      $count = count($bookargs);
      if (is_numeric($bookargs[$count-1]) && $bookargs[$count-2] == 'part') {
        unset($bookargs[$count-2]);
        $result = implode('-', $bookargs);       
      }
    }
  }

  // Remove "question" from page url
  if (count($book_data) > 0) {
    if (substr($result, 0, 8 ) == 'question') {
      $bookargs = explode('-', $result);
      if ( strpos($result, 'question-and-answer') === FALSE
        && strpos($result, 'question-answer') === FALSE
        && strpos($result, 'questions_answers') === FALSE
        && $bookargs[0] != 'questions')
      {
        $result = substr($result, 9);
      }
    }
  }

  // Remove whitespace and extended ascii characters now 
  // Added in v 7.x-1.0 2013-05-28
  $r = '';
  for ($i=0;$i< strlen($result);$i++) {
    $char = substr($result, $i, 1);
    $ord = ord($char);
    if (($ord < 128) and ($ord >= 32)) {
      $r .= $char;
    }
  }
  $result = $r;
  
  // double hyphen '--'
  $result = str_replace('--', '-', $result);
  
  return $result;
}

function custom_process_books_table_exists($tablename) {
  $c39 = chr(39);
  $sql = 'SELECT COUNT(*) ';
  $sql .= 'FROM information_schema.tables ';
  $sql .= 'WHERE table_name = ' . $c39 . $tablename . $c39;
  $result = db_query($sql);
  if ($result) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
*
* convert entities to plain text
* 
*/

function custom_process_books_clean_up_entities($text) {
  $elist = custom_process_books_entity_list();
  $ttext = $text;
  foreach ($elist as $key => $value) {
    $ttext = str_replace($key, $value, $ttext);
  }
  return $ttext;
}


/*
 * Return nid of the node with the source file name provided.
 */

function custom_process_books_get_nid_from_fileid($fileid) {
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
                    ->entityCondition('bundle', 'book')
                    ->propertyCondition('status', 1)
                    ->fieldCondition('field_source_file_name',
                       'value',
                        $filename)
                    ->execute();
  if (!array_key_exists('node', $entities)) {
    return Array();
  }
  $nids = array_keys($entities['node']);
  if (!count($nids)) {
    return Array();
  }
  return $nids;
}

/*
*
* Return a field value for a node directly from the db
* (avoids node_laod)
*
*/

function custom_process_books_get_field_value($nid, $field_name) {
  $result = db_select('node', 'n')
        ->fields('n')
        ->condition('nid', $nid,'=')
        ->execute()
        ->fetchAssoc();
  $vid = 0;
  if (array_key_exists('vid',$result)) {
    $vid = $result['vid'];
  }
  $table = 'field_revision_' . $field_name;
  $field = $field_name . '_value';
  $result = db_select($table, 't')
        ->fields('t')
        ->condition('revision_id', $vid,'=')
        ->execute()
        ->fetchAssoc();
  $value = NULL;
  if (array_key_exists($field, $result)) {
    $value = $result[$field];
  }
  return $value;
}


/*
*
* Return nid of the node from the cite key.
* TODO - could be simplified? all we want is the nid
*/
function custom_process_books_get_nid_from_cite_key($data) {
  $key = '';
  if (is_array($data)) {
    if (array_key_exists('CITE-KEY',$data) && !empty($data['CITE-KEY'])) {
      $key = $data['CITE-KEY'];
    } else {
      return -1;
    }
  }
  else {
    $key = strval($data);
  }
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
                    ->entityCondition('bundle', 'book')
                    ->fieldCondition('field_cite_key',
                       'value',
                        $key)
                    ->execute();
  if (!array_key_exists('node', $entities)) {
    return 0;
  }
  $nids = array_keys($entities['node']);
  if (!count($nids)) {
    return 0;
  }
  return intval($nids[0]);
}

function custom_process_books_import_log(
        $dir,
        $record,
        $message
        )
  {
  $c10 = chr(10);
  $render = '';
  $render .= '"item", "exists", "count", "size", "nid", ';
  $render .= '"file", "path", "title", "ID", ';
  $render .= '"author", "edition", "year", "publisher", ';
  $render .= '"original ID", "key", "genre"' . $c10;
  
  $fp = fopen($dir . '/_data.csv', 'a');
//  fwrite($fp, $render);
  $keys = array('count', 'exists', 'filecount', 'size', 'nid');
  $keys[] = 'FILE';
  $keys[] = 'PATH';
  $keys[] = 'TITLE';
  $keys[] = 'BOOK-NUMBER';
  $keys[] = 'AUTHOR';
  $keys[] = 'ORIGINAL-EDITION';
  $keys[] = 'YEAR';
  $keys[] = 'PUBLISHER';
  $keys[] = 'ORIGINAL-ID';
  $keys[] = 'CITE-KEY';
  $keys[] = 'GENRE';

  $time = date(DATE_ATOM, time());  
  $row = Array($time);
    for ($i = 0; $i <= 4; $i++) {
      $value = 'n/a';
      $key = $keys[$i];
      if (array_key_exists($key, $record)) {
        $value = $record[$key];
      }
      $row[] = '"' . $value . '"';
    }
    $rec = $record['data'];
     for ($i = 5; $i <= 15; $i++) {
      $value = 'n/a';
      $key = $keys[$i];
      if (array_key_exists($key, $rec)) {
        $value = $rec[$key];
      }
      $row[] = '"' . $value . '"';
    }
    $row[] = '"' . $message . '"';
    $row = implode(',', $row);
    fwrite($fp, $row . $c10);
  fclose($fp);
  return;
}


/*
 * Extract the metadata for this book from the org mode PROPERTIES block
 */
function custom_process_books_get_metadata($pages) {
  $metadata = Array();
  $rawtext = $pages[0];
  $linedata = explode('#+', $rawtext);
  foreach( $linedata as $line) {
    // The '2' argument in explode means it only breaks apart the first colon
    $lineinfo = explode(':', $line, 2);
    if (count($lineinfo) < 2) {
      continue;
    }
    $metadata[trim($lineinfo[0])] = trim($lineinfo[1]);
  }
  $title = FALSE;
  if (array_key_exists('TITLE', $metadata)) {
    $title = $metadata['TITLE'];
  }
  $rawtext = $pages[1];
  $linedata = explode(chr(10), $rawtext);
  if (!$title) {
    $metadata['TITLE'] = trim($linedata[0]);
  }
  $ok_to_parse = FALSE;
  foreach( $linedata as $line) {
    $lineinfo = explode(':', $line);
    if (count($lineinfo) < 2) {
      continue;
    }
    if (trim($lineinfo[1]) == 'PROPERTIES') {
      $ok_to_parse = TRUE;
      continue;
    }
    if (trim($lineinfo[1]) == 'END') {
      break;
    }
    if ($ok_to_parse) {
      $key = array_shift($lineinfo);
      $key = strtoupper(trim(array_shift($lineinfo)));
      $value = trim(implode(':', $lineinfo));
      $metadata[$key] = $value;
    }
  }

  if (!empty($pages[0])) {
    
  }


  return $metadata;
}

function find_all_text_files($dir)
{
    $root = scandir($dir);
    foreach($root as $value)
    {
        // Skip all files/directories beginning with . or -
        if(in_array(substr($file_name,0,1), Array('.','-')) || $value === '..') {continue;}
        if(is_file("$dir/$value") && substr("$dir/$value", -4) == '.txt') {$result[]="$dir/$value";continue;}
        foreach(find_all_files("$dir/$value") as $value)
        {
            $result[]=$value;
        }
    }
    return $result;
}

// Drupal is called from a $conf variable which is generated from cache using bootstrap
// However this module is not being recognised properly during bootstrap
function custom_variable_get($variable_name, $default_value) {

    $value = db_query('SELECT value FROM {variable} WHERE name = :name', array(':name' => $variable_name))->fetchField();

    if(!empty($value)) {
       return unserialize($value);
    } else {
       return $default_value;
    }
} 
  



