<?php
/**
 * Basic configuration settings for the module.
 */

/** The maximum page title size in characters */
define('CUSTOM_PROCESS_BOOKS_MAX_TITLE_SIZE', 2047);

/** The maximum page url size in characters */
define('CUSTOM_PROCESS_BOOKS_MAX_URL_SIZE', 100);

# Return an array of entities to be munged
function custom_process_books_entity_list() {
  $elist = Array('&hellip;' => '...');
  $elist['&ndash;'] = ' - ';
  $elist['<i>'] = '';
  $elist['<-i>'] = '';
  $elist['</i>'] = '';
  $elist['&#8217;'] = chr(39);
  $elist['&#8220;'] = chr(34);
  $elist['&#8221;'] = chr(34);
  $elist['<p>#+BEGIN_VERSE</p>'] = '';
  $elist['<p>#+END_VERSE</p>'] = '';
  $elist['\\'] = '';
  
  return $elist;
}

# Return the filesystem address of the data.
function custom_process_books_data_dir($value= '') {
  $ret = custom_process_books_option_get('custom_process_books_data_dir');
  return $ret;
}

# Return the files stream wrapper for the data directory.
function custom_process_books_data_uri() {
  $uri = 'private://library';
  $return = file_stream_wrapper_get_instance_by_uri($uri);
  $return = $return->getUri();
  return $return;
}

# Return the filesystem address of the org mode data.
function custom_process_books_data_org_mode($value= '') {
  return custom_process_books_option_get('custom_process_books_data_org_mode');
}

function custom_process_books_org_mode_metadata_list() {
  $metadatalist = Array();
  $metadatalist[] = 'BOOK-NUMBER';
  $metadatalist[] = 'AUTHOR';
  $metadatalist[] = 'ORIGINAL-EDITION';
  $metadatalist[] = 'YEAR';
  $metadatalist[] = 'PUBLISHER';
  $metadatalist[] = 'ORIGINAL-ID';
  $metadatalist[] = 'CITE-KEY';
  $metadatalist[] = 'GENRE';
  return $metadatalist;
}

// Return a list of unwanted path characters
// used in custom_process_books_make_url_from_text
function custom_process_books_illegal_path_characters() {
  $chars = Array();
  $delete = $replace = Array();
  // space " "
  $replace[] = ' ';
  // comma ,
  $replace[] = ',';
  // period .
  $replace[] = '.';
  // slash /
  $replace[] = chr(47);
  // at @
  $replace[] = '@';
  // ampersand &
  $replace[] = '&';
  // equals =
  $replace[] = '=';
  // colon :
  $replace[] = ':';
  // underscore _
  $replace[] = '_';
  // semi-colon ;
  $replace[] = ';';

  $chars['replace'] = $replace;
  
  // bang !
  $delete[] = chr(33);
  // double quote "
  $delete[] = chr(34);
  // hash #
  $delete[] = chr(35);
  // single quote '
  $delete[] = chr(39);
  // question mark ?
  $delete[] = chr(63);
  // back tick `
  $delete[] = chr(96);
  //    [
  $delete[] = '[';
  //    ]
  $delete[] = ']';
  //    (
  $delete[] = '(';
  //    )
  $delete[] = ')';
  //    {
  $delete[] = '{';
  //    }
  $delete[] = '}';
  
  $delete[] = '<i>';
  
  $delete[] = '<-i>';
  $delete[] = '</i>';
  
  $delete[] = '<sup>';
  
  $delete[] = '<-sup>';
  $delete[] = '</sup>';
  
  $delete[] = '<a-class=footref-name=fnr-1-href=fn-1>';

  $delete[] = '<-a>';
  $delete[] = '</a>';

  $delete[] = '<';
  $delete[] = '>';
  $delete[] = '«';
  $delete[] = '»';
  
  $chars['delete'] = $delete;
  
  return $chars;
}


